package com.multimodule.mainmodule;

import com.multimodule.module1.HelloBean;
import com.multimodule.module1.HelloWorldService;
import com.multimodule.module1.WorldBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MainController {
    @Autowired
    private HelloWorldService helloWorldService;

    @Autowired
    private HelloBean helloBean;

    @Autowired
    private WorldBean worldBean;

    @GetMapping("/hello")
    public String hello(){
        return helloBean.say();
    }

    @GetMapping("/world")
    public String world(){
        return worldBean.say();
    }

    @GetMapping("/helloworld")
    public String helloworld(){
        return helloWorldService.say();
    }
}
