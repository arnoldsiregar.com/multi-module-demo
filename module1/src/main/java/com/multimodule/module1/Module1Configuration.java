package com.multimodule.module1;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "com.multimodule.module1")
@Configuration
@EntityScan(basePackages = "com.multimodule.module1")
public class Module1Configuration {

    @Bean
    public HelloBean helloBean(){
        return new HelloBean();
    }

    @Bean
    public WorldBean worldBean(){
        return new WorldBean();
    }
}
