package com.multimodule.module1;

import org.springframework.stereotype.Service;

@Service
public class HelloWorldService {
    public String say(){
        return "Hello world!";
    }
}
