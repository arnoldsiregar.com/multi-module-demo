package com.multimodule.module1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/module1")
public class Module1Controller {
    @Autowired
    private HelloWorldService helloWorldService;

    @Autowired
    private HelloBean helloBean;

    @Autowired
    private WorldBean worldBean;

    @GetMapping("/hello")
    public String hello(){
        return helloBean.say();
    }

    @GetMapping("/world")
    public String world(){
        return worldBean.say();
    }

    @GetMapping("/helloworld")
    public String helloworld(){
        return helloWorldService.say();
    }
}
